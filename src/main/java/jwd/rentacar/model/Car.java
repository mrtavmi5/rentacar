package jwd.rentacar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tblCar")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @Column
    @NotBlank
    private String model;
    @Column(unique = true)
    @NotBlank
    private String registration;
    @Column
    @NotNull
    private int year;
    @Column
    private double consumption;
    @Column
    private boolean rented;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Company company;

    public Car() {
    }

    public Car(Long id, String model, String registration, int year, double consumption, boolean rented) {
	this.id = id;
	this.model = model;
	this.registration = registration;
	this.year = year;
	this.consumption = consumption;
	this.rented = rented;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getModel() {
	return model;
    }

    public void setModel(String model) {
	this.model = model;
    }

    public String getRegistration() {
	return registration;
    }

    public void setRegistration(String registration) {
	this.registration = registration;
    }

    public int getYear() {
	return year;
    }

    public void setYear(int year) {
	this.year = year;
    }

    public double getConsumption() {
	return consumption;
    }

    public void setConsumption(double consumption) {
	this.consumption = consumption;
    }

    public boolean isRented() {
	return rented;
    }

    public void setRented(boolean rented) {
	this.rented = rented;
    }

    public Company getCompany() {
	return company;
    }

    public void setCompany(Company company) {
	this.company = company;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Car))
	    return false;
	Car other = (Car) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
