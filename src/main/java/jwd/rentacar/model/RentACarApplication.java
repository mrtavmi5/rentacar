package jwd.rentacar.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class RentACarApplication extends SpringBootServletInitializer {

    public static void main(String[] args) throws Exception {
	SpringApplication.run(RentACarApplication.class, args);
    }
}
